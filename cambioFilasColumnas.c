#include <stdio.h>
#include <stdlib.h>

int main() {

    int filas, columnas, fila, columna;
    printf("Ingrese las filas de las matrices\n");
    scanf("%d", &filas);
    printf("Ingrese las columnas de las matrices \n");
    scanf("%d", &columnas);

    int A[filas][columnas];
    int B[filas][columnas];

    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {

            printf("ingrese valores de la matriz A %d%d \n", fila + 1, columna + 1);
            scanf("%d", &A[fila][columna]);
        }
    }

    printf("Los valores de la matriz A son :\n");
    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            printf("[%d]", A[fila][columna]);
        }
        printf("\n");

    }


    printf("Los valores de la matriz B son :\n");
    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            B[fila][columna] = A[columna][fila];
            printf("[%d]", B[fila][columna]);
        }
        printf("\n");
    }

    return 0;
}

